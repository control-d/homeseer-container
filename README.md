# homeseer-container

Container image build artifacts and other files for running HomeSeer 4 container image with podman


## Running the container
podman run --name _\<name\>_ --device _\<zwave_controller\>_ -dt --net=_\<network\>_ -h _\<hostname\>_ -v /etc/localtime:/etc/localtime:ro -v /etc/machine-id:/etc/machine-id:ro -v /opt/HomeSeer/:/opt/HomeSeer/:Z -w /opt/HomeSeer/ quay.io/control-d/homeseer:_\<version\>_

* _\<name\>_: Name for container.  This can be whatever you want
* _\<zwave_controller\>_: Path to Z-Wave USB controller, such as an [Aeotec Z-Stick](https://aeotec.com/z-wave-usb-stick/).  
  * This could be _/dev/ttyUSB0_, _/dev/ttyACM0_, [_/dev/zwave_]](https://community.home-assistant.io/t/making-aeotec-z-wave-stick-static/3716), etc. and will be system specific.  
* _\<network\>_: Podman (CNI) network to use for container
  * See the [Networking](#Networking) section for more details
* _\<hostname\>_: Desired hostname for HomeSeer to use
* _\<version\>_: The full HomeSeer 4 version to run
  * See available tagged versions at https://quay.io/repository/control-d/homeseer?tab=tags

Example:

    podman run --name homeseer --device /dev/ttyACM0 -dt --net=homeseer -h hs.dint.lan -v /etc/localtime:/etc/localtime:ro -v /etc/machine-id:/etc/machine-id:ro -v /opt/HomeSeer/:/opt/HomeSeer/:Z quay.io/control-d/homeseer:latest

If a full install of HomeSeer is not already available on the host in _/opt/HomeSeer_ it will be created the first time the container is started.  If a full install is already available then the system files (but not your data) will be overwritten with the HomeSeer version indicated by the container image version.  This is the exact same method that HomeSeer normally uses to upgrade to newer versions. 

You can omit `-v /opt/HomeSeer/:/opt/HomeSeer/:Z` and a full install of HomeSeer 4 will already be available in the container, but everything will be lost when the container is deleted. 

### Networking
This HomeSeer container expects its own IP address on your network, separate from the container host and not on a NAT bridge.  To accomplish this I have configured podman to use a [CNI ipvlan network with static IP address allocation.](98-homeseer.conflist)  Currently the network allocates a single IP address for use only by the homeseer container.  

    {
      "cniVersion": "0.4.0",
      "name": "homeseer",
      "plugins": [
        {
          "type": "ipvlan",
          "master": "wlp2s0",
          "ipam": {
            "type": "static",
            "addresses": [
              {
                "address": "192.168.0.111/24",
                "gateway": "192.168.0.1"
               }
            ],
    	"routes": [
    	  { "dst": "0.0.0.0/0" }
    	],
            "dns": {
              "nameservers" : ["192.168.0.1"]
            } 
          }
        }
      ]
    }

To configure such a network for podman on your container host copy this config to _/etc/cni/net.d/_, then change the following values:
* `.plugins[0].master`: This is the main networking interface on your system
* `.plugins[0].ipam.addresses`: This is a list of IP addresses and their gateway addresses to be assigned to the container
* `.plugins[0].ipam.dns.nameservers`: This is the list of nameservers to be used in the container

The example above attaches an ipvlan device to the `w1p2s0` wireless network interface and assigns the container an IP address of 192.168.0.111 on my network.  The container will use 192.168.0.1, my home wireless router, as the default network gateway and DNS server.  


## Using the container
The container will run systemd which in turn will start the `homeseer.service`.  Once the service is started HomeSeer will be accessible via the IP address that was previously configured.  

Using `Tools`->`Linux`->`Shutdown System` will gracefully shut down the container (and not your host system).  Optionally, running `podman stop <container>` will tell systemd in the container to stop the container's `homeseer.service`, which will run the `stop_homeseer.sh` script.  The script will attempt to read _/opt/HomeSeer/auth_ and if you have defined the HS admin user and password as `HSUSER` and `HSPASS`, respectively, then it will use HomeSeer's API to gracefully shutdown the process.  

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEwNjg0NDgwMTFdfQ==
-->
