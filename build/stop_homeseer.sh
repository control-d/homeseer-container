#!/bin/bash

source /opt/HomeSeer/auth

/usr/bin/curl --user ${HSUSER}:${HSPASS} --fail 'http://127.0.0.1/LinuxTools' -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' --data 'ConfirmShutdownhs=Yes' --compressed
sleep 10s
