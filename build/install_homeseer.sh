#!/bin/bash

# On first run of container untar HomeSeer into /opt/HomeSeer
# if HomeSeer was not previously installed, this installs it
# if HomeSeer was installed, then this ensures it's updated
if [[ -f /opt/homeseer.tar.gz ]]; then
    cd /opt
    tar xvf /opt/homeseer.tar.gz
    rm -vf /opt/homeseer.tar.gz
    /opt/HomeSeer/install.sh
else
# On subsequent runs of container HomeSeer should already be installed/updated,
# so don't do anything
    echo "HomeSeer already installed/updated"
fi
